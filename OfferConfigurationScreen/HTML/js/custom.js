$(document).ready(function() {
	$('.multi-select-dropdown').multiselect({
		selectAllValue: 'multiselect-all',
		enableCaseInsensitiveFiltering: true,
		enableFiltering: true,
		maxHeight: '300',
		onChange: function(element, checked) {
			var selectedOptionValueArr = this.$select.val();
			console.log(selectedOptionValueArr);
		}
	});
	
	$(".datepicker").datepicker({
		changeMonth: false,
		showOn: "button",
		buttonImage: "img/calendar_icn.png",
		buttonImageOnly: true,
		buttonText: ""
	});

	$('#advanceSearch').on('click', function(){
		$('.advFormGroupFilter').toggleClass('hide');
		$(this).find('.cmnicons').toggleClass('advAct');
	});

	$(document).on('click', '.cancelDataBadges', function(){
		$(this).parent('li').remove();
	});

	$('.flt-mnu-icn').on('click', function(){
		$(this).toggleClass('active');
		$('.floatingActions').toggleClass('hide');
	});

	$('.dropdown-toggle').dropdown();
});